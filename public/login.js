var isOpen = 'isOpen';
var show = 'show';

function toggleLogin(){
    
    var elem = document.getElementById('login');

    if (elem.classList.contains(isOpen)){
        elem.classList.remove(isOpen);
    } else {
        elem.classList.add(isOpen);
    }
}

function onInputKeyUp(element){

    var parent = element.parentElement;
    var isEmpty = element.value.length === 0;
    var hasClass = parent.classList.contains(show)
    
    if (hasClass && isEmpty)
        parent.classList.remove(show);
    else if (!hasClass && !isEmpty)
        parent.classList.add(show);
}