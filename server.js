var express = require('express'),
    browserSync = require('browser-sync')
    app = express();

// configure server
var host = 'localhost'
    port = 3000
    proxy = host + ":" + port;

app.use(express.static(__dirname + '/public'));

app.get('*', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.listen(port, function(){
    
    browserSync({
      proxy: proxy,
      files: ['public/**/*.{html,js,css}'],
      open: false,
      reloadOnRestart: true
    });

    console.log('Listening on: ' + proxy)
});